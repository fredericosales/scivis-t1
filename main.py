#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales <frederico.sales@engenharia.ufjf.br>
Iago Rosa <iago.rosa@engenharia.ufjf.br>
T1 - SciVis

Marching squares algorithm
"""
import numpy as np
import matplotlib.pyplot as plt

from skimage import measure
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage.draw import ellipsoid

# =========================================================================== #
# marching square


def marching_squares(func):
    # Construct some test data
    x, y = np.ogrid[-np.pi:np.pi:100j, -np.pi:np.pi:100j]

    if func == 0:
        r = np.sin(np.exp((np.sin(x)**3 + np.cos(y)**2)))
        img = 'img/sin_contour_finding.png'
    else:
        r = np.cos(np.exp((np.cos(x)**3 + np.sin(y)**2)))
        img = 'img/cos_contour_finding.png'

    # Find contours at a constant value of 0.8
    contours = measure.find_contours(r, 0.8)

    # Display the image and plot all contours found
    fig, ax = plt.subplots()
    ax.imshow(r, interpolation='nearest', cmap=plt.cm.gray)

    for n, contour in enumerate(contours):
        ax.plot(contour[:, 1], contour[:, 0], linewidth=2)

    ax.axis('image')
    ax.set_xticks([])
    ax.set_yticks([])

    plt.savefig(img)
    plt.show()


# =========================================================================== #
# marching cube


def marching_cubes():
    # Generate a level set about zero of two identical ellipsoids in 3D
    ellip_base = ellipsoid(6, 10, 16, levelset=True)
    ellip_double = np.concatenate((ellip_base[:-1, ...],
                                   ellip_base[2:, ...]), axis=0)

    # Use marching cubes to obtain the surface mesh of these ellipsoids
    verts, faces, normals, values = measure.marching_cubes_lewiner(ellip_double, 0)

    # Display resulting triangular mesh using Matplotlib. This can also be done
    # with mayavi (see skimage.measure.marching_cubes_lewiner docstring).
    fig = plt.figure(figsize=(15, 15))
    ax = fig.add_subplot(111, projection='3d')

    # Fancy indexing: `verts[faces]` to generate a collection of triangles
    mesh = Poly3DCollection(verts[faces])
    mesh.set_edgecolor('k')
    ax.add_collection3d(mesh)

    ax.set_xlabel("x-axis: a = 8 per ellipsoid")
    ax.set_ylabel("y-axis: b = 15")
    ax.set_zlabel("z-axis: c = 21")

    ax.set_xlim(0, 24)  # a = 6 (times two for 2nd ellipsoid)
    ax.set_ylim(0, 20)  # b = 10
    ax.set_zlim(0, 32)  # c = 16

    plt.tight_layout()
    plt.savefig('img/marching_cubes.png')
    plt.show()

# =========================================================================== #


if __name__ == "__main__":
    """Do some."""
    marching_squares(0)
    marching_cubes()
